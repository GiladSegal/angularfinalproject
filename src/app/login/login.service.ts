import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import { HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()
export class LoginService {
  
  // Variables
  http:Http;

  login(credentials){
     let options = {
        headers:new Headers({
         'content-type':'application/x-www-form-urlencoded'
        })
     }
    let  params = new HttpParams().append('username', credentials.username).append('password',credentials.password);
    
    let var1 = this.http.post(environment.url + '/login', params.toString(),options);
    console.log(var1);
    return var1;
  }

  constructor(http:Http) {
  	this.http = http;
  }

}
