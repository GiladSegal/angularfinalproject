import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MessagesService } from './../messages.service';
import { FormGroup , FormControl } from '@angular/forms';
import { FormBuilder, Validators} from "@angular/forms";

@Component({
  selector: 'app-create-message',
  templateUrl: './create-message.component.html',
  styleUrls: ['./create-message.component.css']
})
export class CreateMessageComponent implements OnInit {

  @Output() addMessage:EventEmitter <any> = new EventEmitter <any>();
  @Output() addMessagePs:EventEmitter <any> = new EventEmitter <any>();
  user_id;

  service:MessagesService;
  msgform = new FormGroup({
  	  title:new FormControl(null,  Validators.required),
      body:new FormControl(null,  Validators.minLength(5)),
      user_id:new FormControl(),
  });

  sendData(){
  this.addMessage.emit(this.msgform.value.message); 
  console.log(this.msgform.value);
  this.service.postMessage(this.msgform.value).subscribe(
    response =>{
      console.log(response.json())
      this.addMessagePs.emit();
    }

  )
}
  constructor(service:MessagesService) {
    this.service = service;
    this.user_id = localStorage.getItem('user_id');
   }

  ngOnInit() {}

}
